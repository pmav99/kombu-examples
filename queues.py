from kombu import Exchange, Queue

# These are the declarations for the queues and the exchange
# that are shared by both the consumer and worker scripts.as

# Messages are delivered to exchanges which route them to queues
# In this case, our exchane is 'direct' meaning that messages will be routed to a queue who has requested messages
# that match that routing key EXACTLY.
exchange = Exchange(name='example', type='direct', durable=True)

# Exchanges route messages to queues. This queue is bound to our excample exchange and asks to have any messages
# with routing key 'work' delivered to it.
work_queue = Queue('work', exchange=exchange, durable=True, routing_key='work')

# To delay messages for delivery at a later time, we use something called dead-letter exchanges and TTL.
# We specify a dead-letter exchange when we create the queue. If a message's TTL expires or the message is nack'd by
# a consumer, it will be delivered to the x-dead-letter-exchange exchange with the specified dead-letter-routing-key.

# In this case, we want to deliver it back to the same exchange with our same work_queue's routing key
delay_args = {"x-dead-letter-exchange": exchange.name, "x-message-ttl": 10 * 1000, 'x-dead-letter-routing-key': work_queue.name}
delay_queue = Queue('delay_work', exchange=exchange, durable=True, queue_arguments=delay_args, routing_key='delay_work')

AMQP_CONFIG = 'amqp://amqp//'
