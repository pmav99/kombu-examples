The purpose of this project is demonstrate the usage of Kombu for RabbitMQ message sending and delivery

In addition, this project also demonstrates how to delay message delivery to a later date with dead letter exchanges.

# Running
Note: This project requires Boot2Docker to be installed and running as well as docker-compose.

1. Run `docker-compose up -d amqp` to start RabbitMQ
2. Run `docker-compose run example` to run the example.
3. Shut down RabbitMQ by running `docker-compose down`

# What to expect
The example first runs producer.py which generates 5 messages for immediate delivery followed by one message for
delivery at a later time.

Then, the example runs consumer.py which immediately retrieves and processes the first 5 messages from the queue.
After several seconds, you will see the final message delivered and outputted to the console.
