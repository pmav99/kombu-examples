FROM ubuntu

RUN apt-get update && apt-get -y install python3 python3-pip python3-dev
RUN pip3 install pip --upgrade
RUN pip install kombu

ADD . /code
WORKDIR /code

CMD python3 run_example.py
