from datetime import datetime

from kombu import Connection, Consumer
from kombu.mixins import ConsumerMixin

from queues import AMQP_CONFIG, work_queue

queues = [work_queue]

AMQP_CONFIG = 'amqp://amqp//'

class Worker(ConsumerMixin):

    def __init__(self, connection):
        self.connection = connection

    def get_consumers(self, Consumer, channel):
        # The ConsumerMixin's run() method calls get_consumers and begins consuming from the specified consumers
        return [
            Consumer(queues, callbacks=[self.on_message], accept=['json']),
        ]

    def on_message(self, body, message):
        # Print out a log message showing we received the message. Notice that body is the automatically
        # deserialized body of the message
        print("%s RECEIVED MESSAGE: %r" % (datetime.now().isoformat(), body['id']))

        # Here is where we would do some work

        # The message must be acknowledged using message.ack(). This is to make sure that if we crash for some
        # reason, the unacked message can be returned to the queue and processed by another worker.
        message.ack()

def run_worker():

    with Connection(AMQP_CONFIG) as conn:
        Worker(conn).run()

if __name__ == '__main__':
    run_worker()