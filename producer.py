from datetime import datetime

from kombu import Connection

from queues import AMQP_CONFIG, exchange, work_queue, delay_queue

def create_work(msg_id, queue=work_queue):
    # Creates a sample message with the specified msg_id and places it onto the proper queue

    with Connection(AMQP_CONFIG) as conn:
        # Create a producer (this will handle the connection and serialization for us)
        producer = conn.Producer(serializer='json')

        # Publish this message to the exchange. Note, the routing_key is set to the routing_key for
        # the queue that we want to deliver it to.

        producer.publish({'type': 'test', 'id': msg_id},
                          exchange=exchange, routing_key=queue.routing_key,
                          declare=[queue])

        # Note: Please notice the `declare` parameter above. This parameter declares the queue if it does not
        # already exist so we can be sure our messages don't get routed into the void

        print("%s SENT MESSAGE: %r" % (datetime.now().isoformat(), msg_id))

if __name__ == '__main__':

    for i in range(5):
        # Send five example messages
        create_work(i)

    # Send one delay message
    create_work(i + 1, delay_queue)